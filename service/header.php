<?php

?>

<!DOCTYPE html>
<html>
<head>
    <title>Электронное пособие по С++ в среде Qt Creator</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <link rel="stylesheet" type="text/css" href="/style.css" />
</head>
<body>

<div class="sidenav">
    <a href="/signup.php">Регистрация</a>
    <a href="/login.php">Вход</a>
    <a href="about.php">О нас</a>
    <a href="studying.php">Начать обучение</a>
    <a href="/basics/basic_c.html">Основы основ</a>

    <a href="/extra.php">Для продвинутых</a>
    <a href="sources.php">Полезные ресурсы</a>
    <a href="about.php">Об авторе</a>
    <a href="/opros/index.html">Тестирование</a>
    <a href="connect.php">Cвязаться с нами</a>
    <a href="/index.php">На главную</a>
</div>
